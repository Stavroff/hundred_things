import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hundred_things_challenge/myList.dart';
import 'package:hundred_things_challenge/currency.dart';
import 'package:hundred_things_challenge/widgets/my_calendar.dart';
import 'package:url_launcher/url_launcher.dart';

class MainBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {

    return _MainBodyState();
  }
}

class _MainBodyState extends State<MainBody> {
  String urlImg = 'https://st3.depositphotos.com/1001335/13016/i/950/'
      'depositphotos_130161344-stock-photo-a-lot-of-different-things.jpg';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(fontFamily: 'Lobster'),
        home: Scaffold(
            body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(urlImg),
              colorFilter: ColorFilter.linearToSrgbGamma(),
              alignment: Alignment.bottomCenter,
            ),
          ),
          //gradient: LinearGradient(colors: [Colors. white, Colors.lightBlueAccent, Colors.deepPurple, Colors.green, Colors.orange], begin: Alignment.topLeft, end: Alignment.bottomRight)
          child: ListView(
            padding: EdgeInsets.fromLTRB(9.0, 35.0, 2.0, 9.0),
            children: <Widget>[
              Text(
                'Прожить год с сотней вещей!',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20.0, color: Colors.purple),
              ),
              Container(
                  child: Stack(
                children: <Widget>[
                  Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      Image.asset('assets/images/circul.png',
                          color: Colors.green, width: 250.0, height: 250.0),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MyList()));
                        },
                        child: Column(
                          children: <Widget>[
                            Text(
                              'Мой',
                              style: TextStyle(
                                  fontSize: 30.0, color: Colors.white),
                            ),
                            Text(
                              'список',
                              style: TextStyle(
                                  fontSize: 30.0, color: Colors.white),
                            ),
                            Text(
                              'вещей',
                              style: TextStyle(
                                  fontSize: 30.0, color: Colors.white),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Positioned(
                    right: 27.0,
                    top: 22.0,
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Image.asset(
                          'assets/images/circul.png',
                          width: 200.0,
                          height: 200.0,
                          color: Colors.lightBlueAccent,
                        ),
                        InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MyCalendar()));
                            },
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'Мой',
                                  style: TextStyle(
                                      fontSize: 22.0, color: Colors.white),
                                ),
                                Text(
                                  'календарь',
                                  style: TextStyle(
                                      fontSize: 22.0, color: Colors.white),
                                ),

//                            Text(
//                              'вещей',
//                              style:
//                              TextStyle(fontSize: 25.0, color: Colors.white),
//                            ),
                              ],
                            ))
                      ],
                    ),
                  ),
                  Positioned(
                    left: 60.0,
                    top: 167.0,
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Image.asset(
                          'assets/images/circul.png',
                          width: 130.0,
                          height: 130.0,
                          color: Colors.yellow,
                        ),
                        InkWell(
                          onTap: LaunchUrl,
                          child: Column(
                            children: <Widget>[
                              Text(
                                'О',
                                style: TextStyle(
                                    fontSize: 12.0, color: Colors.white),
                              ),
                              Text(
                                'челлендже',
                                style: TextStyle(
                                    fontSize: 12.0, color: Colors.white),
                              ),
//                              Text(
//                                'времени',
//                                style: TextStyle(
//                                    fontSize: 13.0, color: Colors.white),
//                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Positioned(
                    left: 130.0,
                    top: 137.0,
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Image.asset(
                          'assets/images/circul.png',
                          width: 155.0,
                          height: 155.0,
                          color: Colors.purpleAccent,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Currency()));
                          },
                          child: Column(
                            children: <Widget>[
                              Text(
                                'Мои',
                                style: TextStyle(
                                    fontSize: 17.0, color: Colors.white),
                              ),
                              Text(
                                'заметки',
                                style: TextStyle(
                                    fontSize: 17.0, color: Colors.white),
                              ),
//                              Text(
//                                'времени',
//                                style: TextStyle(
//                                    fontSize: 17.0, color: Colors.white),
//                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
                overflow: Overflow.visible,
              )),
              SizedBox(
                height: 100.0,
              ),
              Container(
                  padding: EdgeInsets.only(left: 12.0, right: 12.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Сделай три простых шага:',
                        style: TextStyle(fontSize: 25.0),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Container(
                        //padding: EdgeInsets.fromLTRB(12.0, 0.0, 0.0, 0.0),
                        child: Text(
                            '1. Cократи ненужное из жизни, квартиры и шкафа.'),
                        alignment: Alignment.centerLeft,
                      ),
                      Container(
                        //padding: EdgeInsets.fromLTRB(12.0, 0.0, 0.0, 0.0),
                        child: Text(
                            '2. Откажись от покупки новых ненужных вещей.'),
                        alignment: Alignment.centerLeft,
                      ),
                      Container(
                        //padding: EdgeInsets.fromLTRB(12.0, 0.0, 0.0, 0.0),
                        child:
                            Text('3. Измени жизненные приоритеты и ценности.'),
                        alignment: Alignment.centerLeft,
                      ),
                    ],
                  )),
              SizedBox.fromSize(
                child: SizedBox(
                  height: 75.0,
                ),
              ),
              //Text('Designed by "Who" Ukraine 2019', textAlign: TextAlign.center, style: TextStyle(fontSize: 15.0),)
            ],
          ),
        )));
  }


  // ignore: non_constant_identifier_names
  LaunchUrl() async {
    const url =
        'http://art-mumu.ru/interesnye-materialy/100-things-challenge-prozhit-god-s-sotnej-veshhej';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void ParseTest() {
  }
}
