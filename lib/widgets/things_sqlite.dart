//import 'dart:async';
//import 'package:path/path.dart';
//import 'package:sqflite/sqflite.dart';
//
//void main() async {
//  final database = openDatabase(
//
//    join(await getDatabasesPath(), 'things_database.db'),
//
//    onCreate: (db, version) {
//      return db.execute(
//        "CREATE TABLE dogs(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)",
//      );
//    },
//
//    version: 1,
//  );
//
//  Future<void> insertThings(Things things) async {
//    // Get a reference to the database.
//    final Database db = await database;
//
//    // Insert the Dog into the correct table. Also specify the
//    // `conflictAlgorithm`. In this case, if the same dog is inserted
//    // multiple times, it replaces the previous data.
//    await db.insert(
//      'dogs',
//      things.toMap(),
//      conflictAlgorithm: ConflictAlgorithm.replace,
//    );
//  }
//
//  Future<List<Things>> dogs() async {
//    // Get a reference to the database.
//    final Database db = await database;
//
//    // Query the table for all The Dogs.
//    final List<Map<String, dynamic>> maps = await db.query('dogs');
//
//    // Convert the List<Map<String, dynamic> into a List<Dog>.
//    return List.generate(maps.length, (i) {
//      return Things(
//        id: maps[i]['id'],
//        name: maps[i]['name'],
//        age: maps[i]['age'],
//      );
//    });
//  }
//
//  Future<void> updateThings(Things things) async {
//    // Get a reference to the database.
//    final db = await database;
//
//    // Update the given Dog.
//    await db.update(
//      'things',
//      things.toMap(),
//      // Ensure that the Dog has a matching id.
//      where: "id = ?",
//      // Pass the Dog's id as a whereArg to prevent SQL injection.
//      whereArgs: [things.id],
//    );
//  }
//
//  Future<void> deleteThings(int id) async {
//    // Get a reference to the database.
//    final db = await database;
//
//    // Remove the Dog from the database.
//    await db.delete(
//      'things',
//      // Use a `where` clause to delete a specific dog.
//      where: "id = ?",
//      // Pass the Dog's id as a whereArg to prevent SQL injection.
//      whereArgs: [id],
//    );
//  }
//
//  var fido = Things(
//    id: 0,
//    name: 'Fido',
//    age: 35,
//  );
//
//  // Insert a dog into the database.
//  await insertThings(fido);
//
//  // Print the list of dogs (only Fido for now).
//  print(await dogs());
//
//  // Update Fido's age and save it to the database.
//  fido = Things(
//    id: fido.id,
//    name: fido.name,
//
//  );
//  await updateThings(fido);
//
//  // Print Fido's updated information.
//  print(await dogs());
//
//  // Delete Fido from the database.
//  await deleteThings(fido.id);
//
//  // Print the list of dogs (empty).
//  print(await dogs());
//}
//
//class Things {
//  final int id;
//  final String name;
//  final int age;
//
//  Things({this.id, this.name, this.age});
//
//  Map<String, dynamic> toMap() {
//    return {
//      'id': id,
//      'name': name,
//      'age': age,
//    };
//  }
//
//  // Implement toString to make it easier to see information about
//  // each dog when using the print statement.
//  @override
//  String toString() {
//    return 'Dog{id: $id, name: $name, age: $age}';
//  }
//}