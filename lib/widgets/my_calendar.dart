import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';


class MyCalendar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {

    return _MyCalendarState();
  }


}

class _MyCalendarState extends State<MyCalendar> {

  CalendarController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = CalendarController();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 12.0, left: 5.0, right: 5.0, bottom: 12.0),
        child: Column(
          children: <Widget>[
            TableCalendar(
              calendarController: _controller,
            startingDayOfWeek: StartingDayOfWeek.monday,)
          ],
        ),
      ),
    );
  }
}
