import 'package:flutter/material.dart';
import 'package:hundred_things_challenge/widgets/my_calendar.dart';

void main() => runApp(Calendar());

class Calendar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CalendarState();
  }
}

class CalendarState extends State<Calendar> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyCalendar(),
    );
  }
}