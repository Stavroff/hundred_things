class ThingsModel {
  String id = "1";
  String token;

  ThingsModel({
    this.id,
    this.token,

  });

  factory ThingsModel.fromJson(Map<String, dynamic> json) => new ThingsModel(
    token: json["token"],

  );

  Map<String, dynamic> toJson() => {
    "token": token,

  };
}
