import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hundred_things_challenge/database/dao/things_dao.dart';
import 'package:hundred_things_challenge/database/dao/things_dao.dart' as prefix0;
import 'package:hundred_things_challenge/database/helper.dart';

import 'model/thing_model.dart';

class ThingsListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ThingsListPageState();
  }
}

class ThingsListPageState extends State<ThingsListPage> {
  TextEditingController textController = TextEditingController();
  List<ThingsModel> list = List<ThingsModel>();

  //final _formKey = GlobalKey<FormState>();



  @override
  void initState() {
    super.initState();

    ThingsDao dao = ThingsDao();
    dao.getAll().then((listThings) {
      debugPrint("Things length" + list.length.toString());

      setState(() {
        list = listThings;
      });
    }, onError: (e) {
      print(e);
    });

    //_read();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: onAdd,
        child: Icon(Icons.add),
        backgroundColor: Colors.red,
      ),
      body: Container(
        padding: EdgeInsets.all(9.0),
        child: Column(
          children: <Widget>[
            SizedBox(height: 15.0),
//            TextField(
//              controller: textController,
//              decoration: InputDecoration(
//                hintText: 'Введите название вещи',
//              ),
//            ),
            SizedBox(height: 15.0),
            SizedBox(height: 20.0),
            Expanded(
              child: ListView(
                padding: EdgeInsets.all(10.0),
                children: list.reversed.map((data) {
                  return Dismissible (
                    onDismissed: (direction)
                    {ThingsDao dao = ThingsDao();

                    },
                    key: UniqueKey(),
                    direction: DismissDirection.horizontal,
                    child: ListTile(
                      leading: Icon(Icons.dehaze),
                      title: Text(
                        data.token,
                        style: TextStyle(
                            fontSize: 20.0, fontStyle: FontStyle.italic),
                      ),
                      trailing: IconButton(
                          icon: Icon(Icons.edit), onPressed: (){

                      }),
                    ),
                  )
                  /*Dismissible(
                    key: Key(data.id),
                    child: ListTile(
                      leading: CircleAvatar(
                        child: Text(data.token),
                      ),
                      title: Text(data.token),
                    ),
                  )*/
                  ;
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    //save();
  }

  onAdd() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Form(
              //key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: textController,
                      decoration:
                      InputDecoration(hintText: 'Введите название вещи'),
                    ),
                  ),
//                  Padding(
//                    padding: EdgeInsets.all(8.0),
//                    child: TextFormField(),
//                  ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: RaisedButton(
                          child: Text("Add"),
                          onPressed: () {
                            ThingsDao()
                                .save(ThingsModel(token: textController.text));
                            ThingsDao dao = ThingsDao();

                            dao.getAll().then((listThings) {
                              debugPrint(
                                  "Things length" + list.length.toString());

                              setState(() {
                                list = listThings;
                              });
                            }, onError: (e) {
                              print(e);
                            });

                            textController.text = "";
                          }),
                    ),
                    RaisedButton(
                      child: Text('Clear'),
                      onPressed: deleteAll,
                    ),
                    Padding(
                      padding: EdgeInsets.all(3.0),
                      child: RaisedButton(
                        child: Text('Edit'),
                        onPressed: null,
                      ),
                    )
                  ],
                )

                ],
              ),
            ),
          );
        });
//
  }

  deleteAll() async {
    var database = DBHelper().initDB();
    final db = await database;
    db.rawQuery("Delete from " + "token");
    ThingsDao dao = ThingsDao();

    dao.getAll().then((listThings) {
      debugPrint(
          "Things length" + list.length.toString());

      setState(() {
        list = listThings;
      });
    }, onError: (e) {
      print(e);
    });

  }

  deleteById (int id) async {
    var database = DBHelper().initDB();
    final db = await database;
    db.delete("token", where: "id = ?", whereArgs: [id]);

  }

  read(int id) async {
    var database = DBHelper().initDB();
    final db = await database;
    db.query("token", where: "id = ?", whereArgs: [id]);
  }

  update(int id) async {
    var database = DBHelper().initDB();
    final db = await database;
    var res = await db.update("token", .toMap(),
        where: "id = ?", whereArgs: [id]);
    return res;
  }
}

