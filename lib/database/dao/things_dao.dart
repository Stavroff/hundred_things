import 'package:hundred_things_challenge/database/helper.dart';
import 'package:hundred_things_challenge/model/thing_model.dart';
import 'package:sqflite/sqflite.dart';

class ThingsDao {
  var table = "token";

  save(ThingsModel item) async {
    var database = DBHelper().initDB();
    final db = await database;

  String id = new DateTime.now().millisecondsSinceEpoch.toString();

    var raw = await db.rawInsert(
        "INSERT OR REPLACE Into $table (id,token)"
        " VALUES (?,?)",
        [id, item.token]);
    return raw;
  }

  Future<List<ThingsModel>> getAll() async {
    final database = await _getDataBase();
    var map = await database.query(table);
    List<ThingsModel> list = List<ThingsModel>.from(map.map((i) => _fromMap(i)));
    return list;
  }


  deleteAll() async {
    var database = DBHelper().initDB();
    final db = await database;
    db.rawQuery("Delete from " + "token");
  }


  //TODO example
  deleteById(int id) async {
    var database = DBHelper().initDB();
    final db = await database;
    db.delete(table, where: "id = ?", whereArgs: [id]);
  }


  Future<ThingsModel> getByBarCode(String id) async {
    Future<Database> database = DBHelper().initDB();
    final db = await database;
    List<Map> maps = await db.query(table,
        columns: ["uuid", "data", "status"], //TODO
        where: '"id" = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return _fromMap(maps.first);
    }
    return null;
  }


  _fromMap(Map<String, dynamic> map) {
    String token = map['token'];


    var userProfile = ThingsModel(token: token);

    return userProfile;
  }
  _getDataBase() {
    var database = DBHelper().initDB();
    return database;
  }
}
