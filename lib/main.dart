import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hundred_things_challenge/widgets/main_Body.dart';

void main() => runApp(MaterialApp(
  home: HomePage()));




class HomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomePageState();
  }
  }

class _HomePageState extends State<HomePage>{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
      body: Center(
        child: MainBody(),
      ),
    ));
  }
}