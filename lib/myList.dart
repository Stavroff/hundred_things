import 'package:flutter/material.dart';
import 'package:hundred_things_challenge/my_list_page.dart';

void main() => runApp(MyList());

class MyList extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MyListState();
  }

}

class MyListState extends State<MyList> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: ThingsListPage(),
//      home: Scaffold(
//        body: Center(child: RaisedButton(onPressed: (){
//          Navigator.pop(context);
//        }, child: Text('Назад'))),
//      ),
    );
  }
}

